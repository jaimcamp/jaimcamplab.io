---
title: "Privacy and Data Use"
date: 2019-11-13T09:21:22+01:00
draft: false
hidden: true
---


This blog collects no data.

Regarding data collection and privacy by Gitlab.io you can find information [here](https://gitlab.com/gitlab-org/gitlab-foss/issues/46267)
and in [Gitlab's GDPR Portal](https://about.gitlab.com/gdpr/).
