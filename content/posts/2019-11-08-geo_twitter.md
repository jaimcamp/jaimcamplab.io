---
title: "Geolocation analysis of the twitter hashtags: #AguanteSharp and #fueraSharp"
date: 2019-11-08T16:35:52+01:00
tags: ["Twitter", "Chile2019", "Geolocation"]
draft: false
markup: "mmark"
katex: "true"
---

This is the first post in a series of analyses of tweets and other data, related to the late-2019 
social protests in Chile.


## What's happening in Chile
Since mid-October 2019, a series of protests have been taking place in Chile. 
For a review of the background
and effects of the protests please check its [wiki page](https://en.wikipedia.org/wiki/2019_Chilean_protests).

One interesting point has been the possible role of foreign actors in the protests and violent acts.
This argument was presented by the government and some media outlets (mainly 'La Tercera', see links at the end), stating that Cuban and Venezuelan citizens were behind violent protests and the destruction of several subway stations and trains.

After no proof was presented, the newspaper 'La Tercera' had to retract their statement, but the question about the possible influence of foreigners in the protests stays open.

## Foreigner instigation on Twitter?

Besides the direct involvement of foreigners in the protests, 'La Tercera' referenced two reports,
one internal and a second one from the American intelligence, that stated 
<!--an internal report of the government that showed the involvement -->
the involvement of Venezuelan and even Russian citizens in the spreading of anti-government 
propaganda on Twitter.

This bring us to the question, is there a real instigation by foreigners agents in the current crisis?


## Geolocation analysis of tweets and users

To answer this question an analysis was performed. It focused on tweets and users 
that tweeted or retweeted the hashtags "#AguanteSharp" and
"#fueraSharp". These hashtags are in support of the mayor of Valparaiso Jorge Sharp ("#AguanteSharp") or
against him ("#fueraSharp"). 
The mayor is a central figure in current politics, being Valparaiso one of the main cities in Chile 
and he belongs to the left-wing party "Convergencia Social", which
is an opposition party.

## Tweets analyzed 

Over 200.000 tweets and retweets with the hashtags "#AguanteSharp" (in favor of the mayor) and "#fueraSharp" (against him) were scraped from October 31 07:50 UTC to Nov 04 15:34 UTC. 
For each user the geolocation was obtained, in most cases using the self-reported location feature.
Additionally, a "support" coefficient ($$ \in [-1, 1] $$) was calculated for each user based on use of the hashtags. 
An explanation of the calculation of this coefficient can be found [here](#support-coefficient-calculation).


## Distribution of the users

The users were classified as in favor or against based on their support coefficient: those with a coefficient >= 0.7 were marked as in favor of
the mayor and those with a coefficient <= -0.7 were marked as against.

Users in between were discarded, due to the ambiguity of their support.


### Maps of users

The following maps show the location of the users. Most locations are based on the user location, which have
blue marks, while locations based on the "tweet place" are marked red.

#### In favor of the mayor
<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/map_infavor.html" style="position: absolute; top: 0; left: 0; width: 80%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

#### Against the mayor
<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/map_against.html" style="position: absolute; top: 0; left: 0; width: 80%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Most users are concentrated in three regions: South America, North America and Europe.

If we focus on South America we find that some countries have a ratio of users tweeting against him 
in much larger rates than those in favor, especially comparing these ratios to the values from Chile.


### Against/In Favor ratios for main countries

To study this phenomena we define a new coefficient country-wise: the "support ratio". 
Which was calculated as follows: $$ \frac{\text{Users In Favor}}{\text{Users In Favor} + \text{Users Against}} * 100 $$.

Considering the countries with over 50 users the "support ratios" are:

<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/main_countries.html" style="position: absolute; top: 0; left: 0; width: 80%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

We can see a very strong difference in support for users from South American countries compared to Chileans, North Americans and Europeans.

To see this easily, the number of users in favor were plotted against the number of users against for each country and along with their continent. 
Chile was not included in it, due to its large number of users.

<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/scatter_countries.html" style="position: absolute; top: 0; left: 0; width: 80%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Now we can see a sharp division between the countries by their continent, where South Americans tweet much more against, in particular Venezuelans,
than people located in Europe.

This could be caused by the profile of the users, considering that the location is set by the user and can be related to their country of origin. 
With this **assumption** the following profiles were defined:

* South American: Immigrants from different South Americans countries living in Chile. Mostly with a right-wing political opinion, in particular Venezuelans.
* North American: No strong tendency, closer to the Chilean ratio. 
* Europe: Mostly in favor, possibly strong influenced by Chileans living in Europe with a left-leaning political mindset.

### Venezuelan influence

The main result found was the influence of foreigners in the mentioned hashtags. But contrary to the reports by the government 
no evidence of an anti-government propaganda was discovered, instead South Americans 
have in general a pro-government position, especially from Venezuela, which is the location with the second largest number of users after Chile and only showed a 22% of support of the mayor.

### Code and Data

The code can be found in this [repository](https://gitlab.com/jaimcamp/chile-twitter2019)

The data is hosted on S3:
* Raw data: https://jaimcamp-twitter-analysis1.s3.eu-central-1.amazonaws.com/twitter_stream.json.zip
* DataFrames: https://jaimcamp-twitter-analysis1.s3.eu-central-1.amazonaws.com/twitter_geo_data.h5

### Links:
* Original article: https://twitter.com/Curvaspoliticas/status/1188649397963411457
* Edited article: https://www.latercera.com/nacional/noticia/gobierno-rastrea-rol-venezolanos-twitter/880408/
* Retraction by 'La Tercera': https://www.latercera.com/nacional/noticia/aclaracion-articulo-publicado-la-tercera-error-del-nos-hacemos-cargo/881975/
* Additional article accusing foreigners of violent acts: https://www.latercera.com/nacional/noticia/policia-identifica-uno-los-autores-incendios-estaciones-metro/880381/
* Review of the accusations by another news outlet: https://www.eldesconcierto.cl/2019/10/28/redes-cuestionan-a-la-tercera-por-articulo-donde-vinculan-a-venezolanos-y-cubanos-con-manifestaciones-violentas-en-chile/ 

### Support coefficient calculation
The coefficient was calculated first **per tweet**: 
* +1: if the only present hashtag was "#AguanteSharp", independent of the number of times the hashtag was used.
* -1: if the only present hashtag was "#fueraSharp", independent of the number of times the hashtag was used.
* 0: if both hashtags were present, independent of the number of times each hashtag was used.

Then, for each user the overall "support coefficient" was calculated by taking the mean of the coefficients of all the tweets belonging to that user.
