---
title: "Análisis de la red de tweets con los hashtags #AguanteSharp y #fueraSharp"
date: 2019-11-22T17:23:56+01:00
tags: ["Twitter", "Chile2019", "Networks"]
draft: false
markup: "mmark"
toc: "true"
---

For the English version please click [here](/posts/2019-11-22-network_en).

**Resumen**: El análisis de los usuarios que ocuparon los hashtags #AguanteSharp y #fueraSharp mostró
la existencia de 3 grupos de usuarios: 

* El primer grupo contiene mayoritariamente retweets a favor del alcalde y es el grupo con más usuarios (sobre 16.000) y con una proporción de 3 retweets por usuario, la más baja entre los de 3 grupos.
* El segundo grupo está compuesto por 9.000 usuarios y contiene principalmente retweets contra el alcalde con  una proporción sobre 5 retweets por usuarios.
* El tercer grupo también es clasificado como de oposición al alcalde y es el mas pequeño, con 4.500 usuarios, pero con la mayor cantidad de retweets por usuario: 16 retweets por cuenta.

No se encontraron diferencias en el contenido retwitteado por el segundo y tercer grupo, el cual  
está ligado principalmente a la extrema derecha, sino en el uso de Twitter, en particular la cantidad de retweets
por usuario.
Finalmente se descubrió que el tercer grupo presentaba la mayor tasa de cuentas suspendidas, seguido por el segundo grupo.


## Introducción

Este es el segundo post en la serie de análisis de tweets con los hashtags #AguanteSharp y #fueraSharp (el primero se encuentra [aquí](/posts/2019-11-08-geo_twitter)).
En esta serie estudiamos los tweets para encontrar patrones que puedan señalar posibles 
influencias a través de esta red social. En el primer post se estudió el origen de los tweets, mientras
que en esta ocasión el foco es la búsqueda de grupos de usuarios basados en su retweets y el análisis de estos grupos.

## Metodología

Una red fue creado con los retweets, si un usuario retwittió más de una vez a otro usuario el "peso" de la 
relación en la red aumentará acordemente.
Se obtuvieron los grupos en la red usando el algoritmo de Louvain y se seleccionaron todos los grupos con al menos 5% de los retweets totales.

La clasificación de los usuarios fue hecha utilizando el coeficiente de apoyo, el cual se basa en el uso de los hashtags #AguanteSharp y #fueraSharp. Una descripción 
más detallada se dio en el post anterior, el que se puede ver [aquí](/posts/2019-11-08-geo_twitter#support-coefficient-calculation).

### Datos

Los datos ocupados para ello son los mismo que en la primera entrada: sobre 200.000 tweets obtenidos entre 31 de Octubre 07:50 UTC y 04 de Noviembre 15:34 UTC. 
En este análisis solamente los retweets serán ocupados, por lo que el número de tweets se redujo a menos de 170.000.

## Red de usuarios y grupos

Los datos de los retweets fueron usados para construir una red con sobre 33.000 nodos (usuarios) y 150.000 conexiones (retweets).
Se calcularon 200 diferentes grupos en la red, de los cuales la gran mayoría contenían un número bajo de usuarios y retweets.
Mientras que en 3 grupos, también llamados módulos, se concentró el 97% del total de retweets y usuarios. 
La siguiente tabla muestra los 3 grupos (módulos) y sus principales características.

<div style="position: relative; padding-bottom: 26.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/sharp_table.html" style="position: absolute; top: 0; left: 0; width: 100%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

El primer grupo (módulo 0) contiene principalmente usuarios a favor del alcalde Sharp y es mucho más grande que los otros 2, pero contiene el menor número de retweets 
entre los 3 módulos. 

Los módulos 1 y 2 tienen una posición en contra el alcalde y con una cantidad de usuario retwitteando menor a la del primero grupo, pero
con una cantidad de retweets mucho más grande, en especial el módulo 2. Los usuario del módulo 2 hacen en promedio más de **15** retweets por usuario.

Por el alto número de retweets por usuarios en el grupo 2, vamos a analizar ese grupo y compararlo con el grupo 1 (el otro grupo
contrario al alcalde) y ver que sus diferencias.

### Distribución de los usuarios con muchos retweets

Para mostrar la diferencia en la cantidad de usuarios con un gran número de retweets en la siguiente figura se muestran la distribución de 
los usuarios con más de 50 retweets para cada módulo.
![asd](/posts/twitter1/histo_retweet.png)

## Estudio Módulo 2

Por ser el grupo con la menor cantidad de usuario, pero el mayor número de retweets, las principales características de este grupo son mostradas aquí.

### Usuario retweeteados

Una de las primeras preguntas para analizar es ¿quiénes fueron retweeteados por los usuario de este grupo?

En la siguiente table se muestras los 10 primeros usuarios en ser retweeteados:

<div style="position: relative; padding-bottom: 36.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/sharp_origintable_2.html" style="position: absolute; top: 0; left: 0; width: 70%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Una rápida inspección de estos usuarios mostró una fuerte cercanía a la derecha extrema y en particular al partido político "Acción Republicana", es más, el líder del partido aparece 
en primer lugar de la tabla.

La excepción de ello serían los usuario "bravo_barraza", cuya cuenta ha sido suspendida y por lo tanto no es posible de analizar, y
"sebasepuman".

Las tablas para los otros módulos están disponible aquí: [Grupo 0](/posts/twitter1/sharp_origintable_0.html) y [Grupo 1](/posts/twitter1/sharp_origintable_1.html).

### Principales Tweets del grupo

Esta tabla muestra los 10 tweets más retwitteados por el grupo 2:

<div style="position: relative; padding-bottom: 36.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/sharp_originretweetstable_2.html" style="position: absolute; top: 0; left: 0; width: 100%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Las características de los tweets son muy similares a las de las persona retwitteadas: un círculo cercano a la derecha extrema.

Nuevamente las tablas para los otros módulos están disponible aquí: [Grupo 0](/posts/twitter1/sharp_originretweetstable_0.html) y [Grupo 1](/posts/twitter1/sharp_originretweetstable_1.html).

### Usuario activos del grupo 2

Finalmente está la pregunta ¿quién realiza los retweets?

<div style="position: relative; padding-bottom: 36.25%; height: 0; overflow: hidden;">
<iframe src="/posts/twitter1/sharp_originretweeterstable_2.html" style="position: absolute; top: 0; left: 0; width: 70%; height: 80%; border:0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Nuevamente nos encontramos con usuario ligados a la extrema derecha, pero en su mayoría sin un rol importante en esa colectividad. Al menos 2 cuentas han sido 
suspendidas de esta lista al momento de analizarlas. 

Lo que más llama la atención es el número de retweets llevados a cabo por ellas. Si se les compara con las tablas de los otros módulos (links a continuación) se
ve de inmediato la diferencia en el número de retweets.

Las tablas para los otros módulos están disponible aquí: [Grupo 0](/posts/twitter1/sharp_originretweeterstable_0.html) y [Grupo 1](/posts/twitter1/sharp_originretweeterstable_1.html).
#### Número de cuentas suspendidas
A raíz de los resultados anterior se investigó el número de usuario en cada módulo con sus cuentas suspendidas hasta el día de 24 de Noviembre.

Considerando los 100 primeros usuario por el número de retweets por módulo se encontró que para el "módulo 0" 2 usuarios fueron suspendidos, 
para el "módulo 1" 5 y para el "módulo 2" 8. 

### Similitud entre los módulos 1 y 2

En un principio se supuso que la diferencia entre los grupos 1 y 2 podría ser política: ambos son contrarios al alcalde, pero están situados en diferentes posiciones
del espectro político de la derecha.

Al estudiar los tweets y los usuarios que fueron retwitteados se encontró que eran muy similares entre ambos grupos. 
Para el caso de los top 20 usuario retwiteados se encontraron 12 usuarios en común, para el top 100 este número aumentó a 87 usuarios en común.

Debido a esto se puede suponer que no es la posición política lo que los separa, sino el tipo de usuario y su uso de Twitter, en especial la cantidad de retweets.

## Trabajo Futuro

Como siguiente paso se intentará definir un modelo estadístico para poder encontrar las características principales de los usuarios 
del módulo 2, en especial en comparación con el módulo 1.

Y se intentará verificar si las cuentas en cada módulo son usuarios reales o no.

