---
title: Links for comparison of dashboards
date: 2019-10-09
categories: ["Cloud computing", "Dashboards"]
tags: ["Links", "AWS", "R"]
---

In this first post I'll log the main sources for the following entries regarding frontends for data analysis.

A first step into this is the use of Shiny-server on AWS, the following links are great resources:

- [Running R on AWS](https://aws.amazon.com/blogs/big-data/running-r-on-aws/)
- [Building Our Own Open Source Supercomputer with R and AWS](https://janlauge.github.io/2019/building-our-own-open-source-supercomputer-with-R/)
